package eg.edu.guc.loa.engine.handler;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

public abstract class GameHandler implements MouseListener {

	public void close() {
		
	}
	
	public abstract void mouseClicked(MouseEvent e);

	public void mouseEntered(MouseEvent e) {
	}

	public void mouseExited(MouseEvent e) {
	}

	public void mousePressed(MouseEvent e) {
	}

	public void mouseReleased(MouseEvent e) {
	}

}
