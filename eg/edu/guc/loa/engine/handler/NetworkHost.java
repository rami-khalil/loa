package eg.edu.guc.loa.engine.handler;

import java.awt.event.MouseEvent;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.JOptionPane;

import eg.edu.guc.loa.engine.Point;
import eg.edu.guc.loa.gui.Main;
import eg.edu.guc.loa.gui.panel.BoardPanel;
import eg.edu.guc.loa.gui.panel.PanelFactory;
import eg.edu.guc.loa.gui.piece.Checker;

public class NetworkHost extends GameHandler {

	private ServerSocket serverSocket;
	private Socket connection;
	private OutputStream outputStream;
	private DataOutputStream dataOutputStream;
	private InputStream inputStream;
	private DataInputStream dataInputStream;
	private NetworkHost thisRef;
	private BoardPanel activeBoardPanel;
	private Thread serverThread;

	public NetworkHost() throws IOException {
		thisRef = this;
		serverSocket = new ServerSocket(1337);
		serverThread = new Thread() {
			public void run() {
				try {
					connection = serverSocket.accept();
					outputStream = connection.getOutputStream();
					dataOutputStream = new DataOutputStream(outputStream);
					inputStream = connection.getInputStream();
					dataInputStream = new DataInputStream(inputStream);
					Main.getWorkingFrame().setActivePanel(
							PanelFactory.getNewNetworkPanel(thisRef));
					JOptionPane.getRootFrame().dispose();
					activeBoardPanel = PanelFactory.getBoardPanel();
				} catch (IOException e) {
					close();
				}
			}
		};
		serverThread.start();
	}

	public void close() {
		try {
			Main.getWorkingFrame().setActivePanel(PanelFactory.getMainPanel());
			serverThread.interrupt();
			if (connection != null) {
				connection.close();
			}
			serverSocket.close();
		} catch (IOException e) {
			System.out.println("Error in closing the server");
		}
	}

	private void parallelListen() {
		activeBoardPanel.disableCheckers();
		(new Thread() {
			public void run() {
				listen();
			}
		}).start();
	}

	public String getIpAddr() {
		String hostIPAddress = this.getLocalIP();
		if (hostIPAddress == null) {
			// i don't know about this line, but i hope we won't need it
			hostIPAddress = serverSocket.getInetAddress().getHostName();
		}
		return String.format("Your IP: %s", hostIPAddress);
	}

	// decoding DATA stream
	private void listen() {
		try {
			String data = dataInputStream.readUTF();
			String[] parsed = data.split(",");

			int x1 = Integer.parseInt(parsed[0]);
			int y1 = Integer.parseInt(parsed[1]);
			int x2 = Integer.parseInt(parsed[2]);
			int y2 = Integer.parseInt(parsed[3]);

			Point from = new Point(x1, y1);
			Point to = new Point(x2, y2);

			Checker selectedChecker = activeBoardPanel.getCheckerAt(from);
			Checker targetSpot = activeBoardPanel.getCheckerAt(to);

			activeBoardPanel.setSelectedChecker(selectedChecker);
			activeBoardPanel.setHighlightedMoves();

			Thread.sleep(1000);

			activeBoardPanel.move(selectedChecker, targetSpot);
			activeBoardPanel.enableCheckers();
		} catch (IOException e) {
			close();
		} catch (InterruptedException ex) {
			System.out.println("Error in interrupting the Thread");
		}
	}

	@Override
	public void mouseClicked(MouseEvent e) {
		Checker clicked = (Checker) e.getSource();
		ArrayList<Point> highlightedMoves = activeBoardPanel
				.getHighlightedMoves();
		Checker selectedChecker = activeBoardPanel.getSelectedChecker();

		if (!highlightedMoves.isEmpty()) {
			if (activeBoardPanel.selectedCanMoveTo(clicked)) {
				Checker targetSpot = clicked;
				activeBoardPanel.move(selectedChecker, targetSpot);

				try {
					dataOutputStream.writeUTF(selectedChecker.getPosition()
							+ "," + targetSpot.getPosition());
				} catch (IOException ex) {
					close();
				}

				parallelListen();
				return;
			}
		}

		activeBoardPanel.setSelectedChecker(clicked);
		activeBoardPanel.setHighlightedMoves();
		activeBoardPanel.repaint();
	}

	/*
	 * this code returns the local IP-address of the machine as 192,XXX,XXX,XXX.
	 * It checks all the network interfaces in the PC and ignore any virtual
	 * networks and so on .....(NB. this is a way to make sure that we get the
	 * right IP)
	 */

	public String getLocalIP() {
		String ipOnly = "";
		try {
			Enumeration<NetworkInterface> nifs = NetworkInterface
					.getNetworkInterfaces();
			if (nifs == null) {
				return "";
			}
			while (nifs.hasMoreElements()) {
				NetworkInterface nif = nifs.nextElement();
				
				// We ignore subinterfaces - as not yet needed.
				if (!nif.isLoopback() && nif.isUp() && !nif.isVirtual()) {
					Enumeration<InetAddress> adrs = nif.getInetAddresses();
					while (adrs.hasMoreElements()) {
						InetAddress adr = adrs.nextElement();
						if (adr != null
								&& !adr.isLoopbackAddress()
								&& (nif.isPointToPoint() || !adr
										.isLinkLocalAddress())) {
							String adrIP = adr.getHostAddress();
							String adrName;
							if (nif.isPointToPoint()) {
								adrName = adrIP;
							} else {
								adrName = adr.getCanonicalHostName();
							}
							if (!adrName.equals(adrIP)) {
								return adrIP;
							} else {
								ipOnly = adrIP;
							}
						}
					}
				}
			}
			if (ipOnly.length() == 0) {
				Logger.getLogger(NetworkInterface.class.getName()).log(
						Level.WARNING, "No IP address available");
				ipOnly = null;
			}
			return ipOnly;
		} catch (SocketException ex) {
			Logger.getLogger(NetworkInterface.class.getName()).log(
					Level.WARNING, "No IP address available", ex);
			return null;
		}
	}

}
