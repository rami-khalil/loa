package eg.edu.guc.loa.engine.handler;

import java.awt.event.MouseEvent;
import java.util.ArrayList;

import eg.edu.guc.loa.engine.AI;
import eg.edu.guc.loa.engine.Board;
import eg.edu.guc.loa.engine.Point;
import eg.edu.guc.loa.gui.panel.BoardPanel;
import eg.edu.guc.loa.gui.panel.PanelFactory;
import eg.edu.guc.loa.gui.piece.Checker;

public final class LocalPVE extends GameHandler {

	public static final LocalPVE EASY = new LocalPVE(2);
	public static final LocalPVE MEDIUM = new LocalPVE(3);
	public static final LocalPVE HARD = new LocalPVE(5);

	private AI aiPlayer;

	private LocalPVE(int level) {
		aiPlayer = new AI(level);
	}

	@Override
	public void mouseClicked(MouseEvent e) {
		BoardPanel activeBoardPanel = PanelFactory.getBoardPanel();
		Checker clicked = (Checker) e.getSource();
		ArrayList<Point> highlightedMoves = activeBoardPanel
				.getHighlightedMoves();
		Checker selectedChecker = activeBoardPanel.getSelectedChecker();

		if (!highlightedMoves.isEmpty()) {
			if (activeBoardPanel.selectedCanMoveTo(clicked)) {
				Checker targetSpot = clicked;
				activeBoardPanel.move(selectedChecker, targetSpot);
				// ai no move patch
//				int current = activeBoardPanel.getBoard().getTurn();
				aiDoNextMove();
//				int after = activeBoardPanel.getBoard().getTurn();
//				if (current == after) {
					// Now what? The AI Couldn't Find A Move.
//				}
				return;
			}
		}

		activeBoardPanel.setSelectedChecker(clicked);
		activeBoardPanel.setHighlightedMoves();
		activeBoardPanel.repaint();
	}

	private void aiDoNextMove() {
		BoardPanel activeBoardPanel = PanelFactory.getBoardPanel();
		Board boardEngine = activeBoardPanel.getBoard();

		Point[] r = aiPlayer.nextMove(boardEngine);
		Checker selectedChecker = activeBoardPanel.getCheckerAt(r[0]);
		Checker targetSpot = activeBoardPanel.getCheckerAt(r[1]);

		try {
			Thread.sleep(200);
		} catch (InterruptedException ex) {
			System.out.println();
		}

		activeBoardPanel.setSelectedChecker(selectedChecker);
		activeBoardPanel.setHighlightedMoves();

		try {
			Thread.sleep(1000);
		} catch (InterruptedException ex) {
			System.out.println();
		}
		activeBoardPanel.move(selectedChecker, targetSpot);
	}
}
