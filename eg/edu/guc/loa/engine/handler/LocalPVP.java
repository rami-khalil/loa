package eg.edu.guc.loa.engine.handler;

import java.awt.event.MouseEvent;
import java.util.ArrayList;

import eg.edu.guc.loa.engine.Point;
import eg.edu.guc.loa.gui.panel.BoardPanel;
import eg.edu.guc.loa.gui.panel.PanelFactory;
import eg.edu.guc.loa.gui.piece.Checker;

public class LocalPVP extends GameHandler {
	
	public static final LocalPVP DEFAULT = new LocalPVP();
	
	@Override
	public void mouseClicked(MouseEvent e) {
		BoardPanel activeBoardPanel = PanelFactory.getBoardPanel();
		Checker clicked = (Checker) e.getSource();
		ArrayList<Point> highlightedMoves = activeBoardPanel.getHighlightedMoves();
		Checker selectedChecker = activeBoardPanel.getSelectedChecker();
		
		if (!highlightedMoves.isEmpty()) {
			if (activeBoardPanel.selectedCanMoveTo(clicked)) {
				Checker targetSpot = clicked;
				activeBoardPanel.move(selectedChecker, targetSpot);
				activeBoardPanel.paintImmediately();
				return;
			}
		}
		activeBoardPanel.setSelectedChecker(clicked);
		activeBoardPanel.setHighlightedMoves();
		activeBoardPanel.repaint();
	}
}
