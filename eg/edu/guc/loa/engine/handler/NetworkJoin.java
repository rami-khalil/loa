package eg.edu.guc.loa.engine.handler;

import java.awt.event.MouseEvent;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.util.ArrayList;

import eg.edu.guc.loa.engine.Point;
import eg.edu.guc.loa.gui.Main;
import eg.edu.guc.loa.gui.panel.BoardPanel;
import eg.edu.guc.loa.gui.panel.PanelFactory;
import eg.edu.guc.loa.gui.piece.Checker;

public class NetworkJoin extends GameHandler {

	private String hostIP;
	private Socket connection;
	private OutputStream outputStream;
	private DataOutputStream dataOutputStream;
	private InputStream inputStream;
	private DataInputStream dataInputStream;
	private BoardPanel activeBoardPanel;

	public NetworkJoin(String targetIP) throws IOException {
		hostIP = targetIP;
		// creating Client I/O stream
		connection = new Socket(hostIP, 1337);
		outputStream = connection.getOutputStream();
		dataOutputStream = new DataOutputStream(outputStream);
		inputStream = connection.getInputStream();
		dataInputStream = new DataInputStream(inputStream);

		// setting the board to current view
		Main.getWorkingFrame().setActivePanel(
				PanelFactory.getNewNetworkPanel(this));
		activeBoardPanel = PanelFactory.getBoardPanel();
		parallelListen();
	}

	public void close() {
		try {
			Main.getWorkingFrame().setActivePanel(PanelFactory.getMainPanel());
			if (connection != null && !connection.isClosed()) {
				connection.close();
			}
		} catch (IOException e) {
			System.out.println("Error in closing Connection");
		}
	}

	private void parallelListen() {
		activeBoardPanel = PanelFactory.getBoardPanel();
		activeBoardPanel.disableCheckers();
		(new Thread() {
			public void run() {
				listen();
			}
		}).start();
	}

	private void listen() {
		try {
			String data = dataInputStream.readUTF();
			String[] parsed = data.split(",");

			int x1 = Integer.parseInt(parsed[0]);
			int y1 = Integer.parseInt(parsed[1]);
			int x2 = Integer.parseInt(parsed[2]);
			int y2 = Integer.parseInt(parsed[3]);

			Point from = new Point(x1, y1);
			Point to = new Point(x2, y2);

			activeBoardPanel = PanelFactory.getBoardPanel();

			Checker selectedChecker = activeBoardPanel.getCheckerAt(from);
			Checker targetSpot = activeBoardPanel.getCheckerAt(to);

			activeBoardPanel.setSelectedChecker(selectedChecker);
			activeBoardPanel.setHighlightedMoves();

			Thread.sleep(1000);

			activeBoardPanel.move(selectedChecker, targetSpot);
			activeBoardPanel.enableCheckers();
		} catch (IOException e) {
			close();
		} catch (InterruptedException ex) {
			System.out.println("error in intereptException in Client listen");
		}
	}

	@Override
	public void mouseClicked(MouseEvent e) {
		activeBoardPanel = PanelFactory.getBoardPanel();
		Checker clicked = (Checker) e.getSource();
		ArrayList<Point> highlightedMoves = activeBoardPanel
				.getHighlightedMoves();
		Checker selectedChecker = activeBoardPanel.getSelectedChecker();

		if (!highlightedMoves.isEmpty()) {
			if (activeBoardPanel.selectedCanMoveTo(clicked)) {
				Checker targetSpot = clicked;
				activeBoardPanel.move(selectedChecker, targetSpot);

				try {
					dataOutputStream.writeUTF(selectedChecker.getPosition()
							+ "," + targetSpot.getPosition());
				} catch (IOException ex) {
					close();
				}

				parallelListen();
				return;
			}
		}

		activeBoardPanel.setSelectedChecker(clicked);
		activeBoardPanel.setHighlightedMoves();
		activeBoardPanel.repaint();
	}

}
