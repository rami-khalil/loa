package eg.edu.guc.loa.engine;

public class Point extends java.awt.Point {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public Point() {
		super();
	}

	public Point(int x, int y) {
		super(x, y);
	}
	
	public int getRow() {
		return y;
	}
	
	public int getCol() {
		return x;
	}

	@Override
	public String toString() {
		return String.format("%d,%d", this.x, this.y);
	}
}
