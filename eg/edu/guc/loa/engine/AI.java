package eg.edu.guc.loa.engine;

import java.util.ArrayList;

public class AI {
	private int intelligence;
	private Point[] best;
	private Board state;
	private int nodes = 0;

	public AI(int intelligence) {
		this.intelligence = intelligence;
	}

	public Point[] nextMove(Board state) {
		this.state = state;
		minMax(intelligence, Integer.MIN_VALUE, Integer.MAX_VALUE);
		return best;
	}

	public int getLevel() {
		return intelligence;
	}

	private void minMax(int intelligence, int alpha, int beta) {
		ArrayList<Point[]> moves = state.getAllPossibleMoves(state.getTurn());
		int bestSoFar = Integer.MIN_VALUE;
		int possible = Integer.MIN_VALUE;
		for (Point[] move : moves) {
			state.move(move[0], move[1]);
			possible = minimize(intelligence - 1, alpha, beta);
			// System.out.println(possible + " " + nodes);
			if (possible > bestSoFar) { // is the candidate move better?
				best = move; // take it if it is
				bestSoFar = possible;
			}
			state.undo();
		}
	}

	private int maximize(int intelligence, int alpha, int beta) {
		nodes++;
		if (intelligence == 1 || state.isGameOver()) {
			return state.getValue();
		}
		int max = Integer.MIN_VALUE;
		ArrayList<Point[]> moves = state.getAllPossibleMoves(state.getTurn());
		for (Point[] move : moves) {
			if (alpha >= beta) {
				return alpha;
			}
			state.move(move[0], move[1]);
			max = Math.max(max, minimize(intelligence - 1, alpha, beta));
			alpha = Math.max(alpha, max);
			state.undo();
		}
		return alpha;
	}

	private int minimize(int intelligence, int alpha, int beta) {
		nodes++;
		if (intelligence == 1 || state.isGameOver()) {
			return state.getValue();
		}
		int min = Integer.MAX_VALUE;
		ArrayList<Point[]> moves = state.getAllPossibleMoves(state.getTurn());
		for (Point[] move : moves) {
			if (beta <= alpha) {
				return beta;
			}
			state.move(move[0], move[1]);
			min = Math.min(min, maximize(intelligence - 1, alpha, beta));
			beta = Math.min(beta, min);
			state.undo();
		}
		return beta;
	}

}