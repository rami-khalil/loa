package eg.edu.guc.loa.engine;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Stack;

public class Board implements BoardInterface {

	public static final int NIL = 0, WHITEPLAYER = 1, BLACKPLAYER = 2;
	private int boardSize;
	private int[][] board;
	private boolean[][] dfs;
	private int turn, winner;
	private int[] pieceCount = {40, 12, 12 };
	private boolean[] win = {false, false, false };
	private boolean[] cannotMove = {false, false, false };
	// Representation of all valid traversals from an origin point
	private int[] dr = {1, 1, 1, 0, -1, -1, -1, 0 };
	private int[] dc = {-1, 0, 1, 1, 1, 0, -1, -1 };
	// Board text representations
	private char[] rep = {'-', 'W', 'B' };
	// Play history storage
	private Stack<Point> history;

	private ArrayList<Point> whitePieces;
	private ArrayList<Point> blackPieces;

	public Board() {
		boardSize = 8;
		board = new int[boardSize][boardSize];
		dfs = new boolean[boardSize][boardSize];
		for (int i = 0; i < boardSize - 2; i++) {
			board[0][i + 1] = WHITEPLAYER;
			board[boardSize - 1][i + 1] = WHITEPLAYER;
			board[i + 1][0] = BLACKPLAYER;
			board[i + 1][boardSize - 1] = BLACKPLAYER;
		}
		turn = WHITEPLAYER;
		winner = 0;
		history = new Stack<Point>();
		// drawBoard();
	}

	public Board(int[][] board, int turn) {
		boardSize = board.length;
		this.board = board.clone();
		this.turn = turn;
	}

	public boolean move(Point start, Point end) {
		ArrayList<Point> moves = getPossibleMoves(start);
		// Only allow valid moves when game is still going
		if (!moves.contains(end) || isGameOver()) {
			return false;
		}

		// Store move
		history.push(start);
		history.push(end);
		history.push(new Point(getColor(end), 0));

		// Do move
		board[end.getRow()][end.getCol()] = board[start.getRow()][start
				.getCol()];
		board[start.getRow()][start.getCol()] = NIL;

		// If this was the winning move
		if (isGameOver()) {
			if (win[WHITEPLAYER] && win[BLACKPLAYER]) {
				winner = turn;
			} else if (win[WHITEPLAYER]) {
				winner = WHITEPLAYER;
			} else {
				winner = BLACKPLAYER;
			}
		}

		// System.out.println(String.format("Player %d moved from %s to %s",
		// turn,
		// start, end));
		// drawBoard();
		turn = 3 - turn;
		return true;
	}

	// Count each type of piece on board
	private void countPieces() {
		pieceCount = new int[] {0, 0, 0 };
		// Count of each type of piece
		for (int i = 0; i < boardSize; i++) {
			for (int j = 0; j < boardSize; j++) {
				pieceCount[board[i][j]]++;
			}
		}
	}

	public boolean isGameOver() {
		win[1] = false;
		win[2] = false;
		// Check if a player cannot move
		cannotMove[1] = getAllPossibleMoves(1).size() == 0;
		cannotMove[2] = getAllPossibleMoves(2).size() == 0;
		if (cannotMove[turn]) {
			win[turn] = false;
			win[3 - turn] = true;
			return true;
		}
		// Count how many pieces each player has
		countPieces();
		// Reset visited nodes
		dfs = new boolean[boardSize][boardSize];
		for (int i = 0; i < boardSize; i++) {
			Arrays.fill(dfs[i], false);
		}
		// Check connectivity
		for (int i = 0; i < boardSize; i++) {
			for (int j = 0; j < boardSize; j++) {
				// If the cluster containing this piece
				// contains all pieces of this type
				// then we have a winner.
				if (countClusterDFS(i, j) == pieceCount[board[i][j]]) {
					win[board[i][j]] = true;
				}
			}
		}
		return win[1] || win[2];
	}

	// Count the number of matching pieces in a group/cluster
	private int countClusterDFS(int row, int column) {
		// Don't go out of bounds
		if (!bound(row, column)) {
			return 0;
		}
		// Don't revisit nodes
		if (dfs[row][column]) {
			return 0;
		}
		// Don't visit blank nodes
		if (board[row][column] == NIL) {
			return 0;
		}
		// Mark as visited
		dfs[row][column] = true;
		// Count number of connected pieces from here
		int count = 1;
		for (int i = 0; i < 8; i++) {
			int nextRow = row + dr[i];
			int nextColumn = column + dc[i];
			// Don't go out of bounds
			if (!bound(nextRow, nextColumn)) {
				continue;
			}
			// Don't count opponent pieces.
			if (board[nextRow][nextColumn] != board[row][column]) {
				continue;
			}
			count += countClusterDFS(nextRow, nextColumn);
		}
		return count;
	}

	public int getWinner() {
		return winner;
	}

	public int getColor(Point p) {
		return board[p.getRow()][p.getCol()];
	}

	public int getTurn() {
		return turn;
	}

	// True if the coordinates are within the board limits
	private boolean bound(int r, int c) {
		return r >= 0 && c >= 0 && r < boardSize && c < boardSize;
	}

	public ArrayList<Point> getPossibleMoves(Point start) {
		int row = start.getRow();
		int column = start.getCol();
		ArrayList<Point> moves = new ArrayList<Point>();
		if (board[row][column] != getTurn() || winner != 0) {
			return moves;
		}

		for (int i = 0; i < 4; i++) { // Move in all 4 lines of action
			int o = i + 4; // Opposite direction of movement

			// Count number of pieces on this line of action
			int num = 0;
			num += walk(row, column, i);
			num += walk(row, column, i + 4);
			num -= 1; // Don't double count current piece.

			int r1 = row + num * dr[i];
			int c1 = column + num * dc[i];
			if (validMove(row, column, i, num)) {
				moves.add(new Point(c1, r1));
			}
			// Opposite point
			int r2 = row + num * dr[o];
			int c2 = column + num * dc[o];
			if (validMove(row, column, o, num)) {
				moves.add(new Point(c2, r2));
			}
		}
		return moves;
	}

	// True if moving n steps does not jump over opponent's pieces
	private boolean validMove(int row, int column, int k, int steps) {
		int r1 = row, c1 = column;
		for (int step = 1; step <= steps; step++) {
			r1 += dr[k];
			c1 += dc[k];

			// Don't go out of bounds
			if (!bound(r1, c1)) {
				return false;
			}

			// Don't jump over opponent's pieces
			if (step < steps && board[r1][c1] == 3 - board[row][column]) {
				return false;
			}
		}
		return board[r1][c1] != board[row][column];
	}

	// Count the number of pieces in a straight line
	private int walk(int row, int column, int k) {
		// Don't go out of bounds
		if (!bound(row, column)) {
			return 0;
		}

		// Get next point coordinates
		int nextRow = row + dr[k], nextColumn = column + dc[k];
		// Visit next point
		if (board[row][column] != NIL) {
			return 1 + walk(nextRow, nextColumn, k);
		} else {
			return walk(nextRow, nextColumn, k);
		}
	}

	// Get all possible moves of a player
	protected ArrayList<Point[]> getAllPossibleMoves(int player) {
		ArrayList<Point[]> moves = new ArrayList<Point[]>();
		for (int i = 0; i < boardSize; i++) {
			for (int j = 0; j < boardSize; j++) {
				if (board[i][j] != player) {
					continue;
				}
				Point from = new Point(j, i);
				ArrayList<Point> pmoves = getPossibleMoves(from);
				for (Point to : pmoves) {
					Point[] move = new Point[] {from, to };
					moves.add(move);
				}
			}
		}
		return moves;
	}

	public int[][] getBoard() {
		return board;
	}

	public Board clone() {
		return new Board(board, turn);
	}

	protected int getValue() {
		double result = 0;
		if (isGameOver()) {
			if (winner == 2) {
				return Integer.MAX_VALUE;
			} else if (winner == 1) {
				return Integer.MIN_VALUE;
			}
		} else {
			findAllPieces();
			double artificialPlayer = howClose(turn);
			double humanPlayer = howClose(3 - turn);
			result = humanPlayer - artificialPlayer;
		}
		return (int) result;
	}

	private double howClose(int t) {
		ArrayList<Point> x;
		if (t == WHITEPLAYER) {
			x = whitePieces;
		} else {
			x = blackPieces;
		}
		int length = x.size();
		double sum = 0;
		for (int i = 0; i < length; i++) {
			for (int j = 0; j < i; j++) {
				sum += distanceBetween(x.get(i), x.get(j));
			}
		}
		return sum;
	}

	private double distanceBetween(Point x, Point y) {
		int dr = (int) Math.pow(x.getRow() - y.getRow(), 2);
		int dc = (int) Math.pow(x.getCol() - y.getCol(), 2);
		return Math.sqrt(dr + dc);
	}

	public void findAllPieces() {
		whitePieces = new ArrayList<Point>();
		blackPieces = new ArrayList<Point>();
		for (int i = 0; i < boardSize; i++) {
			for (int j = 0; j < boardSize; j++) {
				if (board[i][j] == 1) {
					whitePieces.add(new Point(j, i));
				} else if (board[i][j] == 2) {
					blackPieces.add(new Point(j, i));
				}
			}
		}
	}

	public void drawBoard() {
		for (int i = 0; i < board.length; i++) {
			for (int j = 0; j < board.length; j++) {
				System.out.print(" " + rep[board[i][j]]);
			}
			System.out.println();
		}
	}

	public boolean undo() {
		if (history.size() == 0) {
			return false;
		}
		// Fetch Move Details
		int val = history.pop().getCol();
		Point end = history.pop();
		Point start = history.pop();
		// Undo
		board[start.getRow()][start.getCol()] = board[end.getRow()][end
				.getCol()];
		board[end.getRow()][end.getCol()] = val;
		turn = 3 - turn;
		winner = 0;
		isGameOver();
		// drawBoard();
		return true;
	}
}
