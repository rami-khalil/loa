package eg.edu.guc.loa.gui.panel;

import java.awt.Dimension;
import java.awt.Point;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.net.URL;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;

public abstract class Panel extends JPanel implements MouseListener {
	private static final long serialVersionUID = -5345824581186190376L;

	protected static final URL MAIN_BACKGROUND_IMAGE = LocalGamePanel.class
			.getResource("GamePanelBG.png");

	protected static final URL BOARD_PANEL_PATH = BoardPanel.class
			.getResource("BoardPanel.png");

	protected static final URL EXIT_IMAGE = MainPanel.class
			.getResource("button/exitGame.png");
	protected static final URL BETWORK_IMAGE = MainPanel.class
			.getResource("button/multiPlayer.png");
	protected static final URL START_IMAGE = MainPanel.class
			.getResource("button/startGame.png");
	protected static final URL UNDO_IMAGE = LocalGamePanel.class
			.getResource("button/Undo.png");
	protected static final URL BACK_IMAGE = LocalGamePanel.class
			.getResource("button/backB.png");

	protected void createLabel(JLabel label, Point point, Dimension dimensions,
			URL path) {
		label.setBounds(point.x, point.y, dimensions.width, dimensions.height);
		if (path != null) {
			label.setIcon(new ImageIcon(path));
		}
		label.addMouseListener(this);
		add(label);
	}

	public void mouseClicked(MouseEvent e) {
	}

	public void mouseEntered(MouseEvent e) {
	}

	public void mouseExited(MouseEvent e) {
	}

	public void mousePressed(MouseEvent e) {
	}

	public void mouseReleased(MouseEvent e) {
	}

}
