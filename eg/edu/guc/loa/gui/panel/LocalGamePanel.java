package eg.edu.guc.loa.gui.panel;

import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Point;
import java.awt.event.MouseEvent;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import eg.edu.guc.loa.engine.handler.GameHandler;
import eg.edu.guc.loa.gui.Main;
import eg.edu.guc.loa.gui.MainFrame;

public class LocalGamePanel extends GamePanel {
	private static final long serialVersionUID = -5978191118867222789L;
	private JLabel undoButton;

	public void paintComponent(Graphics g) {
		ImageIcon icon = new ImageIcon(MAIN_BACKGROUND_IMAGE);
		Image image = icon.getImage();
		g.drawImage(image, 0, 0, getWidth(), getHeight(), null);
	}

	public LocalGamePanel(GameHandler handler) {
		setLayout(null);
		addLabels();
		add(PanelFactory.getNewBoardPanel(handler));
	}

	private void resetBoardPanel() {
		GameHandler handler = PanelFactory.getBoardPanel().getGameHandler();
		remove(PanelFactory.getBoardPanel());
		add(PanelFactory.getNewBoardPanel(handler));
		validate();
	}

	protected void addLabels() {
		super.addLabels();
		undoButton = new JLabel();
		Point point = new Point(0,
				MainFrame.FRAMEHEIGHT / 2 + 25);
		createLabel(undoButton, point, new Dimension(200, 50), UNDO_IMAGE);
	}

	public void displayGameOverMessage(String text) {
		Object[] options = {"Exit", "Undo Last Move", "New Game" };
		int n = JOptionPane.showOptionDialog(Main.getWorkingFrame(), text,
				"~~<< Game Over ! >>~~", JOptionPane.YES_NO_CANCEL_OPTION,
				JOptionPane.QUESTION_MESSAGE, null, options, options[2]);
		if (n == 0) {
			Main.getWorkingFrame().dispose();
		} else if (n == 1) {
			PanelFactory.getBoardPanel().undo();
		} else {
			resetBoardPanel();
		}
	}

	public void mouseClicked(MouseEvent e) {
		if (e.getSource().equals(BACKBUTTON)) {
			if (!PanelFactory.getBoardPanel().isGameOver()) {
				super.backPrompt(null);
			} else {
				Main.getWorkingFrame().setActivePanel(
						PanelFactory.getMainPanel());
			}
		} else if (e.getSource().equals(undoButton)) {
			PanelFactory.getBoardPanel().undo();
		}
	}
}