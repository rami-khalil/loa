package eg.edu.guc.loa.gui.panel;

import eg.edu.guc.loa.engine.handler.GameHandler;

public class PanelFactory {
        private static MainPanel mainPanel;
        private static LocalGamePanel gamePanel;
        private static NetworkGamePanel networkPanel;
        private static BoardPanel boardPanel;
        
        public static MainPanel getMainPanel() {
                if (mainPanel == null) {
                        mainPanel = new MainPanel();
                }
                gamePanel = null;
                networkPanel = null;
                return mainPanel;
        }

        public static void startMainPanel() {
                mainPanel = new MainPanel();
        }

        public static LocalGamePanel getGamePanel() {
                return gamePanel;
        }
        
        public static LocalGamePanel getNewGamePanel(GameHandler handler) {
        		gamePanel = new LocalGamePanel(handler);
        		return gamePanel;
        }
        
        public static BoardPanel getBoardPanel() {
                return boardPanel;
        }
        
        public static BoardPanel getNewBoardPanel(GameHandler handler) {
                boardPanel = new BoardPanel(handler);
                return boardPanel;
        }

        public static NetworkGamePanel getNetworkPanel() {
                return networkPanel;
        }
        
        public static NetworkGamePanel getNewNetworkPanel(GameHandler handler) {
        	networkPanel = new NetworkGamePanel(handler);
        	return networkPanel;
        }

}