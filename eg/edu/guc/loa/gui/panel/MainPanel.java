package eg.edu.guc.loa.gui.panel;

import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Point;
import java.awt.event.MouseEvent;
import java.io.IOException;
import java.net.UnknownHostException;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import eg.edu.guc.loa.engine.handler.LocalPVE;
import eg.edu.guc.loa.engine.handler.LocalPVP;
import eg.edu.guc.loa.engine.handler.NetworkHost;
import eg.edu.guc.loa.engine.handler.NetworkJoin;
import eg.edu.guc.loa.gui.Main;
import eg.edu.guc.loa.gui.MainFrame;

public class MainPanel extends Panel {
	private static final long serialVersionUID = 1L;

	private JLabel startGameL, multiPlayerL, exitL;

	public MainPanel() {
		setLayout(null);
		addLabels();
	}

	public void paintComponent(Graphics g) {
		ImageIcon icon = new ImageIcon(MAIN_BACKGROUND_IMAGE);
		Image image = icon.getImage();
		g.drawImage(image, 0, 0, getWidth(), getHeight(), null);
	}

	public void addLabels() {
		Dimension dimensions = new Dimension(200, 50);

		startGameL = new JLabel();
		int width = (int) (MainFrame.FRAMEWIDTH / 1.05);
		Point point = new Point(MainFrame.FRAMEWIDTH - width,
				MainFrame.FRAMEHEIGHT - 250);
		createLabel(startGameL, point, dimensions, START_IMAGE);

		multiPlayerL = new JLabel();
		point = new Point(MainFrame.FRAMEWIDTH - width,
				MainFrame.FRAMEHEIGHT - 200);
		createLabel(multiPlayerL, point, dimensions, BETWORK_IMAGE);

		exitL = new JLabel();
		point = new Point(MainFrame.FRAMEWIDTH - width,
				MainFrame.FRAMEHEIGHT - 150);
		createLabel(exitL, point, dimensions, EXIT_IMAGE);
	}

	private void newGamePrompt() {
		Object[] options = {"Player VS Player", "Player VS AI" };
		int n = JOptionPane.showOptionDialog(Main.getWorkingFrame(),
				"Choose the type of game", "Local Game",
				JOptionPane.YES_NO_CANCEL_OPTION, JOptionPane.QUESTION_MESSAGE,
				null, options, options[1]);
		if (n == 0) {
			Main.getWorkingFrame().setActivePanel(
					PanelFactory.getNewGamePanel(LocalPVP.DEFAULT));
		} else if (n == 1) {
			singlePlayerPrompt();
		}
	}

	private void singlePlayerPrompt() {
		Object[] options = {"Hard", "Intermediate", "Easy" };
		int n = JOptionPane.showOptionDialog(Main.getWorkingFrame(),
				"Choose how well the AI plays.", "AI Difficulty",
				JOptionPane.YES_NO_CANCEL_OPTION, JOptionPane.QUESTION_MESSAGE,
				null, options, options[2]);
		if (n == 0) {
			Main.getWorkingFrame().setActivePanel(
					PanelFactory.getNewGamePanel(LocalPVE.HARD));
		} else if (n == 1) {
			Main.getWorkingFrame().setActivePanel(
					PanelFactory.getNewGamePanel(LocalPVE.MEDIUM));
		} else if (n == 2) {
			Main.getWorkingFrame().setActivePanel(
					PanelFactory.getNewGamePanel(LocalPVE.EASY));
		}
	}

	private void networkPrompt() {
		Object[] options = {"Host New Game", "Join Existing Game" };
		int n = JOptionPane.showOptionDialog(Main.getWorkingFrame(),
				"Online Play Via IP", "Online Game",
				JOptionPane.YES_NO_CANCEL_OPTION, JOptionPane.QUESTION_MESSAGE,
				null, options, options[1]);
		if (n == 0) {
			hostGamePrompt();
		} else if (n == 1) {
			joinGamePrompt();
		}
	}

	private void joinGamePrompt() {
		String targetIP = JOptionPane.showInputDialog("Target Host IP:");
		try {
			new NetworkJoin(targetIP);
		} catch (UnknownHostException e) {
			JOptionPane.showMessageDialog(Main.getWorkingFrame(),
					"Connection Error: Unkown Host.");
		} catch (IOException e) {
			JOptionPane.showMessageDialog(Main.getWorkingFrame(),
					"Connection Error: Connection Unsuccessful.");
		}
	}

	private void hostGamePrompt() {
		try {
			NetworkHost networkHost = new NetworkHost();
			Object[] options = {"Cancel" };
			int n = JOptionPane.showOptionDialog(null, String.format(
					"Waiting for a Player to join...\n%s",
					networkHost.getIpAddr()), "Online Game",
					JOptionPane.YES_NO_CANCEL_OPTION,
					JOptionPane.QUESTION_MESSAGE, null, options, options[0]);
			if (n == 0) {
				networkHost.close();
			}
		} catch (IOException e1) {
			JOptionPane.showMessageDialog(Main.getWorkingFrame(),
					"Connection Error.");
		}
	}

	public void mouseClicked(MouseEvent e) {
		if (e.getSource().equals(startGameL)) {
			newGamePrompt();
		} else if (e.getSource().equals(multiPlayerL)) {
			networkPrompt();
		} else if (e.getSource().equals(exitL)) {
			System.exit(0);
		}
	}
}