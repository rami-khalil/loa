package eg.edu.guc.loa.gui.panel;

import java.awt.Graphics;
import java.awt.Image;
import java.awt.event.MouseEvent;

import javax.swing.ImageIcon;
import javax.swing.JOptionPane;

import eg.edu.guc.loa.engine.handler.GameHandler;
import eg.edu.guc.loa.gui.Main;

public class NetworkGamePanel extends GamePanel {
	private static final long serialVersionUID = -7014254438321073281L;
	private GameHandler networkHandler;

	public void paintComponent(Graphics g) {
		ImageIcon icon = new ImageIcon(MAIN_BACKGROUND_IMAGE);
		Image image = icon.getImage();
		g.drawImage(image, 0, 0, getWidth(), getHeight(), null);
	}

	public NetworkGamePanel(GameHandler handler) {
		networkHandler = handler;
		setLayout(null);
		addLabels();
		add(PanelFactory.getNewBoardPanel(networkHandler));
	}

	public void displayGameOverMessage(String text) {
		JOptionPane.showMessageDialog(Main.getWorkingFrame(), text);
	}

	public void mouseClicked(MouseEvent e) {
		if (e.getSource().equals(BACKBUTTON)) {
			if (!PanelFactory.getBoardPanel().isGameOver()) {
				super.backPrompt(networkHandler);
			} else {
				networkHandler.close();
			}
		}
	}
}
