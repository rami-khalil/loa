package eg.edu.guc.loa.gui.panel;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Point;

import javax.swing.JLabel;
import javax.swing.JOptionPane;

import eg.edu.guc.loa.engine.handler.GameHandler;
import eg.edu.guc.loa.gui.Main;
import eg.edu.guc.loa.gui.MainFrame;

public abstract class GamePanel extends Panel {
	private static final long serialVersionUID = 5910653664261199527L;
	protected static final JLabel PLAYERTURNLABEL = new JLabel();
	protected static final JLabel BACKBUTTON = new JLabel();

	public abstract void displayGameOverMessage(String message);
	
	protected void addLabels() {
//		backButton = new JLabel();
		Point point = new Point(0,
				MainFrame.FRAMEHEIGHT / 2 - 25);
		createLabel(BACKBUTTON, point, new Dimension(200, 50), BACK_IMAGE);

//		playerTurnL = new JLabel();
		point = new Point(MainFrame.FRAMEWIDTH - 160,
				MainFrame.FRAMEHEIGHT - 50);
		PLAYERTURNLABEL.setForeground(Color.WHITE);
		createLabel(PLAYERTURNLABEL, point, new Dimension(150, 20), null);
	}

	protected void backPrompt(GameHandler game) {
		Object[] options = {"Yes.", "No." };
		int n = JOptionPane.showOptionDialog(this,
				"Are you sure you want to leave the game ? ",
				"Quit", JOptionPane.YES_NO_CANCEL_OPTION,
				JOptionPane.QUESTION_MESSAGE, null, options, options[1]);
		if (n == 0) {
			if (game != null) {
				game.close();
			} else {
				Main.getWorkingFrame().setActivePanel(
						PanelFactory.getMainPanel());
			}
		}
	}

	protected static void changePlayerTurn(int playerTurn) {
		switch (playerTurn) {
		case 1:
			PLAYERTURNLABEL.setText("it is now the Jedi's Turn");
			break;
		case 2:
			PLAYERTURNLABEL.setText("it is now the Sith's Turn");
			break;
		default:
			break;
		}
	}

}
