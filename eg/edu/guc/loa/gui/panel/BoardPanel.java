package eg.edu.guc.loa.gui.panel;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.GridLayout;
import java.util.ArrayList;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;

import eg.edu.guc.loa.engine.Board;
import eg.edu.guc.loa.engine.Point;
import eg.edu.guc.loa.engine.handler.GameHandler;
import eg.edu.guc.loa.engine.handler.LocalPVE;
import eg.edu.guc.loa.engine.handler.LocalPVP;
import eg.edu.guc.loa.gui.Main;
import eg.edu.guc.loa.gui.MainFrame;
import eg.edu.guc.loa.gui.piece.Checker;

public class BoardPanel extends Panel {

	private static final long serialVersionUID = 1L;

	private static final ImageIcon BOARD_PANEL_IMAGE = new ImageIcon(
			BOARD_PANEL_PATH);

	private GameHandler gameHandler;

	private Checker[][] boardCheckers = new Checker[8][8];
	private ArrayList<Point> highlightedMoves = new ArrayList<Point>();
	private static final Color[] SOURCE_HIGHLIGHT_COLOR = {null,
			new Color(33, 114, 255), new Color(255, 119, 151) };
	private static final Color[] HIGHTLIGHT_COLOR = {null,
			new Color(109, 172, 255), new Color(255, 184, 200) };
	private Checker selectedChecker;
	private Board boardEngine = new Board();
	private int top = 0, right = 0, bot = 0, left = 0;

	public ArrayList<Point> getHighlightedMoves() {
		if (selectedChecker == null) {
			highlightedMoves.clear();
		}
		return highlightedMoves;
	}

	public void setHighlightedMoves() {
		if (selectedChecker == null) {
			highlightedMoves.clear();
		} else {
			Point start = selectedChecker.getPosition();
			highlightedMoves = boardEngine.getPossibleMoves(start);
		}
		paintImmediately();
	}

	public void enableCheckers() {
		for (int i = 0; i < boardCheckers.length; i++) {
			for (int j = 0; j < boardCheckers.length; j++) {
				boardCheckers[i][j].addMouseListener(gameHandler);
			}
		}
	}

	public void disableCheckers() {
		for (int i = 0; i < boardCheckers.length; i++) {
			for (int j = 0; j < boardCheckers.length; j++) {
				boardCheckers[i][j].removeMouseListener(gameHandler);
			}
		}
	}

	public void move(Checker from, Checker to) {
		Point start = from.getPosition();
		Point end = to.getPosition();
		boardEngine.move(start, end);
		selectedChecker = null;
		setHighlightedMoves();
		gameOver();
		setPlayerTurnTextL(gameHandler, boardEngine.getTurn());
	}

	private void gameOver() {
		if (boardEngine.isGameOver()) {
			((GamePanel) getParent()).displayGameOverMessage(getWinnerPlayer());
		}
	}

	public boolean isGameOver() {
		return boardEngine.isGameOver();
	}

	public String getWinnerPlayer() {
		int winner = boardEngine.getWinner();
		switch (winner) {
		case 1:
			return "The Jedi Win!";
		case 2:
			return "The Sith Win!";
		default:
			return "I win!";
		}
	}

	public Board getBoard() {
		return boardEngine;
	}

	public void setHighlightedBorder(Checker highlighted) {
		int row = highlighted.getPosition().getRow();
		int col = highlighted.getPosition().getCol();
		try {
			if (boardCheckers[row - 1][col].getBorder() == null) {
				top = 1;
			}
		} catch (Exception e) {
			top = 0;
		}
		try {
			if (boardCheckers[row][col + 1].getBorder() == null) {
				right = 1;
			}
		} catch (Exception e) {
			right = 0;
		}
		try {
			if (boardCheckers[row + 1][col].getBorder() == null) {
				bot = 1;
			}
		} catch (Exception e) {
			bot = 0;
		}
		try {
			if (boardCheckers[row][col - 1].getBorder() == null) {
				left = 1;
			}
		} catch (Exception e) {
			left = 0;
		}
	}

	public void paintComponent(Graphics g) {
		// Draw Background
		g.drawImage(BOARD_PANEL_IMAGE.getImage(), 0, 0, getWidth(),
				getHeight(), null);
		// Reset Highlights
		for (int i = 0; i < boardCheckers.length; i++) {
			for (int j = 0; j < boardCheckers.length; j++) {
				boardCheckers[i][j].setOpaque(false);
				boardCheckers[i][j].setBorder(null);
			}
		}

		// Draw Highlights
		for (Point point : highlightedMoves) {
			Checker highlighted = boardCheckers[point.getRow()][point.getCol()];
			highlighted.setBackground(HIGHTLIGHT_COLOR[boardEngine.getTurn()]);
			highlighted.setOpaque(true);
			setHighlightedBorder(highlighted);
			highlighted.setBorder(BorderFactory.createMatteBorder(top, left,
					bot, right, Color.BLACK));
		}

		if (!highlightedMoves.isEmpty() && selectedChecker != null) {
			selectedChecker.setBackground(SOURCE_HIGHLIGHT_COLOR[boardEngine
					.getTurn()]);
			setHighlightedBorder(selectedChecker);
			selectedChecker.setBorder(BorderFactory.createMatteBorder(top,
					left, bot, right, Color.BLACK));
			selectedChecker.setOpaque(true);
		}
	}

	public boolean selectedCanMoveTo(Checker c) {
		return highlightedMoves.contains(c.getPosition());
	}

	public void undo() {
		selectedChecker = null;
		highlightedMoves.clear();
		if (!getBoard().undo()) {
			JOptionPane.showMessageDialog(Main.getWorkingFrame(),
					"There Are No Moves To Undo !", "Error",
					JOptionPane.ERROR_MESSAGE);
		}
		setPlayerTurnTextL(gameHandler, boardEngine.getTurn());
		repaint();
	}

	public Checker getSelectedChecker() {
		return selectedChecker;
	}

	public void setSelectedChecker(Checker checker) {
		selectedChecker = checker;
	}

	public Checker getCheckerAt(Point p) {
		return boardCheckers[p.getRow()][p.getCol()];
	}

	public void paintImmediately() {
		paintImmediately(0, 0, MainFrame.FRAMEWIDTH, MainFrame.FRAMEHEIGHT);
	}

	public GameHandler getGameHandler() {
		return gameHandler;
	}

	private void setPlayerTurnTextL(GameHandler handler, int playerTurn) {
		if (handler instanceof LocalPVE || handler instanceof LocalPVP) {
			LocalGamePanel.changePlayerTurn(playerTurn);
		} else {
			NetworkGamePanel.changePlayerTurn(playerTurn);
		}
	}

	public BoardPanel(GameHandler handler) {
//		System.out.println("New board panel with handler: " + handler);
		gameHandler = handler;
		setLayout(new GridLayout(8, 8));
		setBounds((MainFrame.FRAMEWIDTH - 800) / 2,
				(MainFrame.FRAMEHEIGHT - 600) / 2, 800, 600);
		setOpaque(false);
		// Create Board
		for (int i = 0; i < boardCheckers.length; i++) {
			for (int j = 0; j < boardCheckers.length; j++) {
				boardCheckers[i][j] = new Checker(new Point(j, i));
				boardCheckers[i][j].addMouseListener(gameHandler);
				add(boardCheckers[i][j]);
			}
		}
		setPlayerTurnTextL(gameHandler, boardEngine.getTurn());
	}

}