package eg.edu.guc.loa.gui;

public class Main {
	private static MainFrame mainFrame;
	
	public static void main(String[] args) {
		mainFrame = new MainFrame(2);
	}
	
	public static MainFrame getWorkingFrame() {
		return mainFrame;
	}
	
	public static void restart(int screenMode) {
		mainFrame.dispose();
		mainFrame = new MainFrame(screenMode);
	}
}
