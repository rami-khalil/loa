package eg.edu.guc.loa.gui;

public class GameIcon extends javax.swing.ImageIcon {
	private static final long serialVersionUID = -9015370333672858931L;
	private static java.net.URL imageURL = GameIcon.class.getResource("Icon.png");
	public GameIcon() {
		super(imageURL);
	}
}
