package eg.edu.guc.loa.gui.piece;

import java.awt.Graphics;
import java.awt.Image;
import java.net.URL;

import javax.swing.ImageIcon;
import javax.swing.JLabel;

import eg.edu.guc.loa.engine.Point;
import eg.edu.guc.loa.gui.panel.PanelFactory;

public class Checker extends JLabel {
	private static final long serialVersionUID = 6413216627561685816L;
	private static final URL WHITE_IMAGE_URL = Checker.class
			.getResource("WhiteChecker.png");
	private static final ImageIcon WHITE_IMAGE_ICON = new ImageIcon(WHITE_IMAGE_URL);
	private static final URL BLACK_IMAGE_URL = Checker.class
			.getResource("BlackChecker.png");
	private static final ImageIcon BLACK_IMAGE_ICON = new ImageIcon(BLACK_IMAGE_URL);
	private Point position;

	public Checker(Point p) {
		position = p;
		setVisible(true);
	}

	public Point getPosition() {
		return position;
	}

	public void setPosition(Point p) {
		position = p;
	}

	private Image getWhiteImage() {
		return WHITE_IMAGE_ICON.getImage();
	}

	private Image getBlackImage() {
		return BLACK_IMAGE_ICON.getImage();
	}

	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		int xDelta = getWidth() / 2 - getWhiteImage().getWidth(null) / 2;
		int yDelta = getHeight() / 2 - getWhiteImage().getHeight(null) / 2;
		int type = PanelFactory.getBoardPanel().getBoard().getColor(position);
		if (type == 1) {
			g.drawImage(getWhiteImage(), xDelta, yDelta, null);
		} else if (type == 2) {
			g.drawImage(getBlackImage(), xDelta, yDelta, null);
		}
	}
}
