package eg.edu.guc.loa.gui;

import java.awt.DisplayMode;
import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;
import java.awt.Window;

import javax.swing.JFrame;

public class Screen {
	// Object for Graphics device
	private GraphicsDevice vc;
	private static final DisplayMode[] DISPLAY_MODES = {
			new DisplayMode(1920, 1080, 32, 0),
			new DisplayMode(1920, 1080, 24, 0),
			new DisplayMode(1920, 1080, 16, 0),
			new DisplayMode(1366, 800, 32, 0),
			new DisplayMode(1366, 800, 24, 0),
			new DisplayMode(1366, 800, 16, 0),
			new DisplayMode(1366, 768, 32, 0),
			new DisplayMode(1366, 768, 24, 0),
			new DisplayMode(1366, 768, 16, 0),
			new DisplayMode(1280, 768, 32, 0),
			new DisplayMode(1280, 768, 24, 0),
			new DisplayMode(1280, 768, 16, 0),
			new DisplayMode(1024, 768, 32, 0),
			new DisplayMode(1024, 768, 24, 0),
			new DisplayMode(1024, 768, 16, 0),
			new DisplayMode(800, 600, 32, 0), new DisplayMode(800, 600, 24, 0),
			new DisplayMode(800, 600, 16, 0) };

	public Screen() {
		// get all Graphics Options
		GraphicsEnvironment env = GraphicsEnvironment
				.getLocalGraphicsEnvironment();
		// Graphics device have access to the screen
		vc = env.getDefaultScreenDevice();
	}

	private DisplayMode selectCompatibleMode() {
		DisplayMode[] currentCompatable = vc.getDisplayModes();
		for (DisplayMode dm : DISPLAY_MODES) {
			for (DisplayMode displayMode : currentCompatable) {
				if (displayModeMach(dm, displayMode)) {
					return displayMode;
				}
			}
		}
		return null;
	}

	private boolean displayModeMach(DisplayMode dm1, DisplayMode dm2) {
		if (dm1.getWidth() != dm2.getWidth()
				|| dm1.getHeight() != dm2.getHeight()) {
			return false;
		}
		if (dm1.getBitDepth() != dm2.getBitDepth()
				&& dm1.getBitDepth() != DisplayMode.BIT_DEPTH_MULTI
				&& dm2.getBitDepth() != DisplayMode.BIT_DEPTH_MULTI) {
			return false;
		}
		if (dm1.getRefreshRate() != dm2.getRefreshRate()
				&& dm2.getRefreshRate() != DisplayMode.REFRESH_RATE_UNKNOWN
				&& dm1.getRefreshRate() != DisplayMode.REFRESH_RATE_UNKNOWN) {
			return false;
		}

		return true;

	}

	public void setFullScreen(JFrame window) {
		DisplayMode dm = selectCompatibleMode();
		window.setUndecorated(true);
		vc.setFullScreenWindow(window);

		if (dm != null && vc.isDisplayChangeSupported()) {
			try {
				vc.setDisplayMode(dm);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	public Window getFullScreenWindow() {
		return vc.getFullScreenWindow();
	}

	public int getCurrentFrameWidth() {
		Window w = vc.getFullScreenWindow();
		if (w != null) {
			return w.getWidth();
		}
		return 0;
	}

	public int getCurrentFrameHeight() {
		Window w = vc.getFullScreenWindow();
		if (w != null) {
			return w.getHeight();
		}
		return 0;
	}

	// Close Game no needed i guess
	public void restoreScreen() {
		Window w = vc.getFullScreenWindow();
		if (w != null) {
			w.dispose();
		}
		vc.setFullScreenWindow(null);
	}
}
