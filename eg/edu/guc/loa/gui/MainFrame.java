
package eg.edu.guc.loa.gui;
import java.awt.Dimension;
import java.awt.Toolkit;

import javax.swing.JFrame;
import javax.swing.JPanel;

import eg.edu.guc.loa.gui.panel.PanelFactory;

public class MainFrame extends JFrame {
	private static final long serialVersionUID = 1L;
	public static final int FRAMEWIDTH = 1200;
	public static final int FRAMEHEIGHT = 700;

	private int screenMode;
	private Screen sc;
	private Dimension dimension = Toolkit.getDefaultToolkit().getScreenSize();

	public MainFrame(int screenMode) {
		new JFrame();
		this.screenMode = screenMode;
		setScreen(screenMode);
		setActivePanel(PanelFactory.getMainPanel());
	}

	public Screen getScreen() {
		return sc;
	}

	public int getScreenMode() {
		return screenMode;
	}

	private void setScreen(int x) {
		setTitle("Lines Of Action");
		setResizable(false);
		setIconImage(new GameIcon().getImage());
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		switch (x) {
		case 1: // Full Screen
			setVisible(false);
			sc = new Screen();
			try {
				sc.setFullScreen(this);
//				FRAMEWIDTH = sc.getCurrentFrameWidth();
//				FRAMEHEIGHT = sc.getCurrentFrameHeight();
			} catch (Exception e) {
				System.out.println("Error in creating Fullscreen");
			}
			break;
		case 2: // Windowed;
//			FRAMEWIDTH = 1200;
//			FRAMEHEIGHT = 700;
			// FRAMEWIDTH = dimension.width;
			// FRAMEHEIGHT = dimension.height;
			// setBounds(0, 0, dimension.width, dimension.height);
			setBounds(((dimension.width - FRAMEWIDTH) / 2),
					((dimension.height - FRAMEHEIGHT) / 2), FRAMEWIDTH,
					FRAMEHEIGHT);
			setVisible(true);
			break;
		default:
			break;
		}

	}

	public void setActivePanel(JPanel p) {
		setContentPane(p);
		validate();
	}
}